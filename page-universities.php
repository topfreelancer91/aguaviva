<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */
// переменные ACF
		get_field('field_577a3efe6dcd1');
		get_field('field_577a3d61d2004');
		get_field('field_57877af4d7777');
		get_field('field_57877b38f2d73');

		$allUniversities = getSortedUniversities($_GET['level'], $_GET['place'], $_GET['subject']);

get_header(); ?>
	<div class="universities-head">
		<div class="container">
			<h1><?php echo the_field('university_h3'); ?></h1>
			<p><?php echo the_field('university_title'); ?></p>
		</div>
	</div>
	<!-- search university -->
	<section class="search-university-catalog">
	<?php get_template_part( 'template-parts/content', 'search-university' ); ?>
	</section>
		<div class="search-result container clearfix">
		<h3><?php _e( 'Результаты поиска', 'aguaviva' )?></h3>
		<p><?php _e( 'Lorem под результатами поиска', 'aguaviva' )?></p>
			<div id="search-result" class="row">
				<?php foreach ($allUniversities as $value): ?>
					<div class="col-xs-12 col-sm-6 col-md-4">
						<div class="university-loop">
							<span class="theme-color-font icon-point-hover point icons-university"><?php echo $value->place; ?></span>
							<span class=" icon-hat level icons-university"><?php echo $value->level; ?></span>
							<span class=" icon-star rating icons-university"><?php echo $value->rating; ?></span>
							<img class="thumbnail-img" src="<?php echo $value->thumbnail; ?>" alt="" />
							<div class="wrap-single-university">
								<h4><?php echo $value->title; ?></h4>
								<a class="btn-style-form btn-search-margin-center" href="<?php echo $value->link;?>"><?php _e( 'подробнее', 'aguaviva' )?></a><br>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>

	<!-- .search university -->

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

		<div class="container">
		<?php
			if ( have_posts() ) : ?>

				<header class="page-header">
					<?php
						the_archive_description( '<div class="taxonomy-description">', '</div>' );
					?>
				</header><!-- .page-header -->

				<?php
				/* Start the Loop */
				while ( have_posts() ) : the_post();

					/*
					 * Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme, then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
		//	get_template_part( 'template-parts/content', get_post_format() );

				endwhile;

				the_posts_navigation();

			else :

				get_template_part( 'template-parts/content', 'none' );

			endif; ?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
