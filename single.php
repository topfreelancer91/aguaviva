<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package aguaviva
 */
bac_PostViews(get_the_ID());
get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <div class="container"><?php if(function_exists('bcn_display'))
			    	{
			    	    bcn_display();
			    	}?></div>
			</div>
		<div class="container">
		<div class="row"><div class="col-sm-12 col-md-8 col-lg-8">
				<?php
				while ( have_posts() ) : the_post();

					get_template_part( 'template-parts/content', get_post_format() );


					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;

				endwhile; // End of the loop.
				?>
				</div>
				<?php get_sidebar(); ?></div>
		</div>
		<!-- modal img -->
			<?php get_template_part( 'template-parts/content', 'modal' ); ?>
		<!-- .modal img -->
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
