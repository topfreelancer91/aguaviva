<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aguaviva
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">


	<!--Output popup-->
  <p><?php
        $selected = get_field('facebook_popup');
        if ($selected) {
          if( in_array('show', $selected) ) {
	             get_template_part( 'template-parts/content', 'modal' );}
        }?>
	</p>


		<p>© 2016 agua viva - <?php _e( 'набор студентов на обучение в Украине. <a class="arr" href="/политика-конфиденциальности/">Все права защищены.</a>', 'aguaviva' ) ?> </p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page .wrapper -->

<?php wp_footer(); ?>

</body>
</html>
