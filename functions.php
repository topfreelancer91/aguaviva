<?php
/**
* aguaviva functions and definitions.
*
* @link https://developer.wordpress.org/themes/basics/theme-functions/
*
* @package aguaviva
*/

if ( ! function_exists( 'aguaviva_setup' ) ) :
	/**
	* Sets up theme defaults and registers support for various WordPress features.
	*
	* Note that this function is hooked into the after_setup_theme hook, which
	* runs before the init hook. The init hook is too late for some features, such
	* as indicating support for post thumbnails.
	*/
	function aguaviva_setup() {
		/*
		* Make theme available for translation.
		* Translations can be filed in the /languages/ directory.
		* If you're building a theme based on aguaviva, use a find and replace
		* to change 'aguaviva' to the name of your theme in all the template files.
		*/
		load_theme_textdomain( 'aguaviva', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		* Let WordPress manage the document title.
		* By adding theme support, we declare that this theme does not use a
		* hard-coded <title> tag in the document head, and expect WordPress to
		* provide it for us.
		*/
		add_theme_support( 'title-tag' );

		/*
		* Enable support for Post Thumbnails on posts and pages.
		*
		* @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		*/
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'aguaviva' ),
			) );

			/*
			* Switch default core markup for search form, comment form, and comments
			* to output valid HTML5.
			*/
			add_theme_support( 'html5', array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			) );

			// Set up the WordPress core custom background feature.
			add_theme_support( 'custom-background', apply_filters( 'aguaviva_custom_background_args', array(
				'default-color' => 'ffffff',
				'default-image' => '',
			) ) );
		}
	endif;
	add_action( 'after_setup_theme', 'aguaviva_setup' );

	// фильтр передает переменную $template - путь до файла шаблона. Изменяя этот путь мы изменяем файл шаблона.

	/**
	* Set the content width in pixels, based on the theme's design and stylesheet.
	*
	* Priority 0 to make it available to lower priority callbacks.
	*
	* @global int $content_width
	*/
	function aguaviva_content_width() {
		$GLOBALS['content_width'] = apply_filters( 'aguaviva_content_width', 640 );
	}
	add_action( 'after_setup_theme', 'aguaviva_content_width', 0 );

	/**
	* Register widget area.
	*
	* @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
	*/
	function aguaviva_widgets_init() {
		register_sidebar( array(
			'name'          => esc_html__( 'Sidebar', 'aguaviva' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'aguaviva' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
			) );
			register_sidebar( array(
				'name'          => esc_html__( 'top-contacts', 'aguaviva' ),
				'id'            => 'top-contacts',
				'description'   => esc_html__( 'Add widgets here.', 'aguaviva' ),
				'before_widget' => '<section id="%1$s" class="widget %2$s">',
				'after_widget'  => '</section>',
				'before_title'  => '',
				'after_title'   => '',
				) );
				register_sidebar( array(
					'name'          => esc_html__( 'lang', 'aguaviva' ),
					'id'            => 'lang',
					'description'   => esc_html__( 'Add widgets here.', 'aguaviva' ),
					'before_widget' => '<section id="%1$s" class="widget %2$s">',
					'after_widget'  => '</section>',
					'before_title'  => '',
					'after_title'   => '',
					) );
				}
				add_action( 'widgets_init', 'aguaviva_widgets_init' );

				/**
				* Enqueue scripts and styles.
				*/
				function aguaviva_scripts() {
					wp_enqueue_style( 'aguaviva-style', get_stylesheet_uri() );

					wp_enqueue_script('jquery');

					wp_enqueue_script( 'bootstrap.min.js', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '3.3.6', true );

					// Main js script with validation form to require on function.php with ajax
					wp_enqueue_script( 'main.js', get_template_directory_uri() . '/js/main.js', array(), '1.0', true );

					wp_localize_script( 'main.js', 'ajax_object',
					array( 'ajax_url' => admin_url( 'admin-ajax.php' ),
					'nonce' => wp_create_nonce('myajax-nonce')
					) );

					wp_localize_script( 'main.js', 'objectL10n', array(
							'level' => esc_html__( 'ВЫБЕРИТЕ УРОВЕНЬ', 'aguaviva' ),
							'sort' => esc_html__( 'СОРТИРОВАТЬ РЕЗУЛЬТАТЫ ПО АЛФАВИТУ', 'aguaviva' ),
							'place' => esc_html__( 'ВЫБЕРИТЕ РАСПОЛОЖЕНИЕ', 'aguaviva' ),
							'subject' => esc_html__( 'ВЫБЕРИТЕ СПЕЦИАЛЬНОСТЬ', 'aguaviva' ),
							'name_university' => esc_html__( 'ВВЕДИТЕ НАЗВАНИЕ ВУЗА', 'aguaviva' ),
							'read_more' => esc_html__( 'подробнее', 'aguaviva' ),
					) );

					// Script of stylish selects
					wp_enqueue_script( 'select2.js', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js');


					wp_enqueue_style( 'select2-style', 'https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css' );



					wp_enqueue_script( 'jquery.maskedinput-1.2.2.js', get_template_directory_uri() . '/js/jquery.maskedinput-1.2.2.js');

					wp_enqueue_script( 'aguaviva-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '1.0', true );

					wp_enqueue_script( 'aguaviva-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '1.0', true );

					if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
						wp_enqueue_script( 'comment-reply' );
					}
					wp_enqueue_style( 'custom', '/wp-content/themes/aguaviva/custom.css');
				}
				add_action( 'wp_enqueue_scripts', 'aguaviva_scripts' );



				/**
				* Implement the Custom Header feature.
				*/
				require get_template_directory() . '/inc/custom-header.php';

				/**
				* Custom template tags for this theme.
				*/
				require get_template_directory() . '/inc/template-tags.php';

				/**
				* Custom functions that act independently of the theme templates.
				*/
				require get_template_directory() . '/inc/extras.php';

				/**
				* Customizer additions.
				*/
				require get_template_directory() . '/inc/customizer.php';

				/**
				* Load Jetpack compatibility file.
				*/
				require get_template_directory() . '/inc/jetpack.php';


				// Implement bootstrap menu
				class Bootstrap_Walker_Nav_Menu extends Walker_Nav_Menu {

					/**
					* Display Element
					*/
					function display_element( $element, &$children_elements, $max_depth, $depth, $args, &$output ) {
						$id_field = $this->db_fields['id'];

						if ( isset( $args[0] ) && is_object( $args[0] ) )
						{
							$args[0]->has_children = ! empty( $children_elements[$element->$id_field] );

						}

						return parent::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
					}

					/**
					* Start Element
					*/
					function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
						if ( is_object($args) && !empty($args->has_children) )
						{
							$link_after = $args->link_after;
							$args->link_after = ' <b class="caret"></b>';
						}

						parent::start_el($output, $item, $depth, $args, $id);

						if ( is_object($args) && !empty($args->has_children) )
						$args->link_after = $link_after;
					}

					/**
					* Start Level
					*/
					function start_lvl( &$output, $depth = 0, $args = array() ) {
						$indent = str_repeat("t", $depth);
						$output .= "\n$indent<ul class=\"dropdown-menu list-unstyled\">\n";
					}
				}
				add_filter('nav_menu_link_attributes', function($atts, $item, $args) {
					if ( $args->has_children )
					{
						$atts['data-toggle'] = 'dropdown';
						$atts['class'] = 'dropdown-toggle';
					}

					return $atts;
				}, 10, 3);
				// -//Implement bootstrap menu
				// fucking white space before html

				// .fucking white space before html
				// breadcrumbs


				// end
				// .breadcrumbs


				//  Function `contactUs()` send email to admin and user from form on main page
				//   AJAX action : `contact_us`
				//   POST Data :
				//       ~ `name`      =>  name of user
				//       ~ `email`     =>  email of user
				// 			 ~ `telephone` =>  user telephone on format +(999) 999-999-999
				//       ~ `text`      =>  text sending by user

				add_action( 'wp_ajax_contact_us', 'contactUs' );
				add_action('wp_ajax_nopriv_contact_us', 'contactUs');

				function contactUs(){

					// Sending email to admin

					$admin_email = get_bloginfo( 'admin_email' );
					$admin_subject = get_post_meta(71,'subject', true);
					$admin_headers[] = 'From: '.$_POST['name'].' <'.$_POST['email'].'>';
					$admin_headers[] = 'content-type: text/html';

					$admin_message = get_post_meta(71, 'text', true);
					$meaning = array(
						'[NAME]'        => $_POST['name'],
						'[FROM]'        => $_POST['email'],
						'[TELEPHONE]'   => $_POST['telephone'],
						'[TEXT]'        => $_POST['text'],
						'[ADMIN_EMAIL]' => $admin_email
					);

					$admin_message = patternText($admin_message, $meaning); // send to function patering email text
					wp_mail( $admin_email, $admin_subject, $admin_message, $admin_headers );

					// Sending email to user

					$user_email = $_POST['email'];
					$user_subject = get_post_meta(72,'subject', true);
					$user_headers[] = 'From: Agua viva <'.$admin_email.'>';
					$user_headers[] = 'content-type: text/html';

					$user_message = get_post_meta(72, 'text', true);
					$user_message = patternText($user_message, $meaning); // send to function patering email text
					wp_mail( $user_email, $user_subject, $user_message, $user_headers );

          //$sms_text = get_post_meta(77, 'sms_text', true);
					//sendSMS($sms_text , "+380936990451"); //send sms to Admin
				}
				// ****************************** END OF FUNCTION ******************************


				//Function `getConsultation()` get consultation
				//   AJAX action : `get_consultation`
				//   POST Data :
				//       ~ `name`      =>  name of user
				//       ~ `email`     =>  email of user
				// 			 ~ `telephone` =>  user telephone on format +(999) 999-999-999

				add_action( 'wp_ajax_get_consultation', 'getConsultation' );
				add_action('wp_ajax_nopriv_get_consultation', 'getConsultation');

				function getConsultation(){

					// Sending email to admin

					$admin_email = get_bloginfo( 'admin_email' );
					$admin_subject = get_post_meta(123,'subject', true);
					$admin_headers[] = 'From: '.$_POST['name'].' <'.$_POST['email'].'>';
					$admin_headers[] = 'content-type: text/html';

					$admin_message = get_post_meta(123, 'text', true);
					$meaning = array(
						'[NAME]'        => $_POST['name'],
						'[FROM]'        => $_POST['email'],
						'[TELEPHONE]'   => $_POST['telephone'],
						'[ADMIN_EMAIL]' => $admin_email
					);

					$admin_message = patternText($admin_message, $meaning); // send to function patering email text
					wp_mail( $admin_email, $admin_subject, $admin_message, $admin_headers );

					// Sending email to user

					$user_email = $_POST['email'];
					$user_subject = get_post_meta(124,'subject', true);
					$user_headers[] = 'From: Agua viva <'.$admin_email.'>';
					$user_headers[] = 'content-type: text/html';

					$user_message = get_post_meta(124, 'text', true);
					$user_message = patternText($user_message, $meaning); // send to function patering email text
					wp_mail( $user_email, $user_subject, $user_message, $user_headers );

          //$sms_text = get_post_meta(77, 'sms_text', true);
					//sendSMS($sms_text , "+380936990451"); //send sms to Admin

				}
				// ****************************** END OF FUNCTION ******************************


				//Function `enroll()` enroll on consultation
				//   AJAX action : `enroll`
				//   POST Data :
				//       ~ `name`      =>  name of user
				//       ~ `email`     =>  email of user
				// 			 ~ `telephone` =>  user telephone on format +(999) 999-999-999

				add_action( 'wp_ajax_enroll', 'enroll' );
				add_action('wp_ajax_nopriv_enroll', 'enroll');

				function enroll(){

					// Sending email to admin

					$admin_email = get_bloginfo( 'admin_email' );
					$admin_subject = get_post_meta(181,'subject', true);
					$admin_headers[] = 'From: '.$_POST['name'].' <'.$_POST['email'].'>';
					$admin_headers[] = 'content-type: text/html';

					$admin_message = get_post_meta(181, 'text', true);
					$meaning = array(
						'[NAME]'        => $_POST['name'],
						'[FROM]'        => $_POST['email'],
						'[TELEPHONE]'   => $_POST['telephone'],
						'[ADMIN_EMAIL]' => $admin_email
					);

					$admin_message = patternText($admin_message, $meaning); // send to function patering email text
					wp_mail( $admin_email, $admin_subject, $admin_message, $admin_headers );

					// Sending email to user

					$user_email = $_POST['email'];
					$user_subject = get_post_meta(182,'subject', true);
					$user_headers[] = 'From: Agua viva <'.$admin_email.'>';
					$user_headers[] = 'content-type: text/html';

					$user_message = get_post_meta(182, 'text', true);
					$user_message = patternText($user_message, $meaning); // send to function patering email text
					wp_mail( $user_email, $user_subject, $user_message, $user_headers );

					var_dump($_POST);

					//$sms_text = get_post_meta(77, 'sms_text', true);
					//sendSMS($sms_text , "+380936990451"); //send sms to Admin
				}
        // ****************************** END OF FUNCTION ******************************


				// Function of pattering text
				function patternText( $pattern, $meaning ){
					foreach ($meaning as $key => $value) {
						$pattern = str_replace($key, $value, $pattern);
					}
					return $pattern;
				}
   			// ****************************** END OF FUNCTION ******************************


        // Function send sms on mobile number with parameters :
				//   ~ `sms_text`  = text of sms (max 70 chars on kirilick);
				//   ~ `telephone` = telephone number of user who get sms;
				// Function return data on XML format with status code
				// API DOC with code error and example (http://alphasms.ua/storage/files/alphasms-http-api.pdf)

				function sendSMS($sms_text, $telephone){

					$xml_data ='<?xml version="1.0" encoding="utf-8" ?>'.
					             '<package key="ddcb9a7369ed70d476bed3e0626824161bf557f3" >'.
				            	   '<message>'.
					                 '<msg recipient= "'.$telephone.'" sender= "AguaViva" type="0">'.$sms_text.'</msg>'.
					               '</message>'.
				              	'</package>';

					$URL = "https://alphasms.ua/api/xml.php";
					$ch = curl_init($URL);
					curl_setopt($ch, CURLOPT_MUTE, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
					curl_setopt($ch, CURLOPT_POSTFIELDS, "$xml_data");
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					$output = curl_exec($ch);
					curl_close($ch);

					return $output;
				}
				// ****************************** END OF FUNCTION ******************************


				// Function get all subjects on all univercities

				function getAllUnicSubjects(){
					$allSubjects = array();

					$qry_args = array(
						'post_status' => 'publish',
						'post_type' => 'universities',
						'posts_per_page' => -1,
					);
         $all_posts = new WP_Query( $qry_args );

				 foreach ( $all_posts->posts as $value ) {

					 $university_subjects =  get_field( 'subjects' , $value->ID , true );

					 if ( $university_subjects != false ){
					 		foreach ( $university_subjects as $subject ) {
								if( isUnicSubject( $allSubjects, $subject ) ){
									$allSubjects[] = (object) [
										'departmentID' =>  $subject['subject_number'],
										'department' 	 =>	 $subject['department']
									];
								}
					 		}
				 		}
				 }

					return $allSubjects;
				}
				// ****************************** END OF FUNCTION ******************************


				// Function return true when array hasn't value

				function isUnicSubject ( $array, $subject) {
					foreach ($array as $value) {
						if($value->departmentID == $subject['subject_number']){
								return false;
						}
					}
					return true;
				}
				// ****************************** END OF FUNCTION ******************************


				// Function sort array on tree parameters

				add_action( 'wp_ajax_get_sorted_universities', 'getSortedUniversities' );
				add_action('wp_ajax_nopriv_get_sorted_universities', 'getSortedUniversities');

				function getSortedUniversities ($level, $place, $subject){

						$universities = array();

						if (isset($_POST['action'])){
							$level = $_POST['level'];
							$place = $_POST['place'];
							$subject = $_POST['subject'];
						}

						$qry_args = array(
								'post_status' => 'publish',
								'post_type' => 'universities',
								'posts_per_page' => -1,
						);
						$all_posts = new WP_Query( $qry_args );

						foreach ( $all_posts->posts as $value ) {
							if( $level != '' ){
								$university_level =  get_field( 'level' , $value->ID , true );
								if($university_level != $level){
									continue;
								}
							}

							if( $place != '' ){
								$university_place =  get_field( 'place' , $value->ID , true );
								if($university_place != $place){
									continue;
								}
							}

							if( $subject != '' ){
								$university_subjects =  get_field( 'subjects' , $value->ID , true );
								if ( $university_subjects != false ){
									$universityHasSunbject = false;
									 foreach ( $university_subjects as $university_subject ) {
										 if ($university_subject['subject_number'] == $subject){
											 $universityHasSunbject = true;
											 break;
										 }
									 }

									 if(!$universityHasSunbject){
										 continue;
									 }
								 }
							}
							 $universities[] =  (object) [
							 		'Id'        => $value->ID,
							 		'title'     => $value->post_title,
							    'level'     => get_field( 'level' , $value->ID , true ),
									'rating'    => get_field( 'rating' , $value->ID , true ),
									'place'     => __(get_field( 'place' , $value->ID , false ), 'aguaviva'),
									'thumbnail' => get_field( 'slider' , $value->ID , true )[0]["picture"],
									'link'      => get_permalink($value->ID)
							  ];
						}

					if (isset($_POST['action'])){
						echo json_encode($universities); die();
					} else {
						return $universities;
					}

				}
				// ****************************** END OF FUNCTION ******************************

				// Read more btn

				function replace_excerpt($content) {
					return str_replace(	'Read more','
														  	<a class="btn-read" href="'. get_permalink() .'">Читать</a>',
																	$content
																);
				}

				// ****************************** END OF FUNCTION ******************************

				add_filter('the_excerpt', 'replace_excerpt');

				//Set the Post Custom Field in the WP dashboard as Name/Value pair

				function bac_PostViews($post_ID) {

			    		$count_key = 'post_views_count';
			        $count = get_post_meta($post_ID, $count_key, true);

			    		if($count == ''){

			        		$count = 0;

			        		delete_post_meta($post_ID, $count_key);

			        		add_post_meta($post_ID, $count_key, '0');

			        		return $count . '';
			    		}else{
			        		$count++;

			        		update_post_meta($post_ID, $count_key, $count);

			        		if($count == '1'){
			        				return $count . '';
			        		} else {
			        				return $count . '';
			        		}
			    		}
					}

					// ****************************** END OF FUNCTION ******************************
// add custom thumbnail size
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'recent-post', 75, 75, true ); 
}
