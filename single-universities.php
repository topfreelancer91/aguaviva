<?php
/**
 * The template for displaying all single posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package aguaviva
 */
// переменные ACF
		get_field('field_577cc655a49bb');
		get_field('field_577cc6f5b7684');
		get_field('field_577df56ed2bda');
		get_field('field_577df67f8b891');
		get_field('field_577df871b6554');
		get_field('field_57a0ac49170fb');
get_header(); ?>
<?php
	$POST_ID = get_the_ID();

  $subjects = get_field('subjects');
	$countries = get_field('priznanie');
	$slider = get_field('slider');
	$video_slider = get_field('video');

	//  foreach ($video_slider as $value) {
	// 	$value['video'] = 'video';
	//  	array_unshift($slider, $value);
	//  }

	 array_splice( $slider, 1, 0, $video_slider );
	$i = 0;

	?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="breadcrumbs" typeof="BreadcrumbList" vocab="http://schema.org/">
			    <div class="container"><?php if(function_exists('bcn_display'))
			    	{
			    	    bcn_display();
			    	}?></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="single-university-h3">
						<h3><?php echo get_the_title(); ?></h3>
					</div>
					<div class="single-university clearfix">
						<div id="slider" class="col-xs-12 col-sm-12 col-md-9">
						                        <!-- Top part of the slider -->
	                            <div class="col-xs-12" id="carousel-bounding-box">
	                                <div class="carousel slide" id="gallery-university">
	                                    <!-- Carousel items -->
	                                    <div class="carousel-inner">
											<?php foreach($slider as $slide) : ?>
		                                        <div class="<?php echo ($i == 0) ? 'active' : ''; ?> item" data-slide-number="<?php echo $i; ?>">
																							<?php if(isset($slide["link_on_video"])) : ?>
																									<video class="video-slide" controls>
																											<source src="<?php echo $slide["link_on_video"] ?>" type="video/mp4">
																											<?php _e( 'Ваш браузер не поддерживает html5.', 'aguaviva' )?>
																									</video>
																							<?php else :  ?>
																								<div class="img-slide" style="background: url(<?php echo $slide["picture"] ?>)"></div>
																						<?php endif; ?>
																					  </div>
		                                        <?php $i++; ?>
		                                    <?php endforeach; ?>
	                                    </div>
	                                </div>
	                            </div>

			                <div class="col-xs-12 hidden-xs" id="slider-thumbs">
			                        <!-- Bottom switcher of slider -->
				                         	<div class="carousel slide" id="gallery-university-thumb">
		                                   		<div class="carousel-inner overflow-toggle">
							                        <?php $i = 0; for($j = 1; $j <= ceil( count($slider)/4); $j++):?>
					                                   			<div class="<?php echo ($j == 1) ? 'active' : ''; ?> item" data-slide-number="<?php echo $j; ?>">
									                        		<ul class="hide-bullets">
																		<?php for($k = 0; $k < 4; $k++ ) : ?>
									                          					<li class="col-sm-3">
									                          						<?php if($slider[$i] ["picture"] != null || isset($slider[$i]["link_on_video"])) : ?>
							                            								<a class="thumbnail" id="carousel-selector-<?php echo $i; ?>">

																														<?php if(isset($slider[$i]["link_on_video"])) : ?>
																																	<div class="video-tmb"></div>
																														<?php else :  ?>
																																	<img class="slide-tmb" src="<?php echo $slider[$i] ["picture"] ?>" alt="" />
																														<?php endif; ?>
							                                      			 			</a>
							                                      					<?php endif; ?>
									                            				</li>
									                            		 	<?php $i++; ?>
										                       			<?php endfor; ?>
									                        		</ul>
								                        		</div>
							                    	<?php endfor;?>
					                          	</div>
		                           			</div>

	                                    <!-- Carousel nav -->
	                                    <a class="left carousel-control" href="#gallery-university-thumb" role="button" data-slide="prev">
	                                        <span class="glyphicon glyphicon-chevron-left"></span>
	                                    </a>
	                                    <a class="right carousel-control" href="#gallery-university-thumb" role="button" data-slide="next">
	                                        <span class="glyphicon glyphicon-chevron-right"></span>
	                                    </a>
			                </div>
						</div><!--/Slider-->
						<div class="side col-xs-12 col-sm-12 col-md-3">
							<ul>
								<li>
									<span class="side-s icon-hat-s icons-university"><?php _e( 'Уровень акредитации', 'aguaviva' )?>:</span>  <a href="/universities?level=<?php echo get_post_meta($POST_ID, 'level', true); ?>"><?php echo get_post_meta($POST_ID, 'level', true);?></a>
								</li>
								<li>
									<span class="side-s icon-point-s icons-university"><?php _e( 'город', 'aguaviva' )?>:</span><a href="/universities?place=<?php echo get_post_meta($POST_ID, 'place', true); ?>"><?php _e(get_post_meta($POST_ID, 'place', true), 'aguaviva');?></a>
								</li>
						        <li>
						        	<span class="side-s icon-star-s icons-university"><?php _e( 'рейтинг', 'aguaviva' )?> :</span> <?php echo get_post_meta($POST_ID, 'rating', true);?>
						        </li>
								<li>
									<span class="side-s icon-megaphone-s icons-university"><?php _e( 'признание в других странах', 'aguaviva' )?> :</span>
								</li>
								<li class="country overflow-h">
									<?php
										foreach ($countries as $country) {
											echo $country["country"] . "<br>";
										}

									?>
								</li>
																<?php echo (count($countries) > 2) ? '<a href="#" id="more">'. __( "смотреть все", "aguaviva" ) .'</a>' : ''; ?>
							</ul>
							<p>
							   <?php echo get_field('single_page_university'); ?>
							</p>
						</div>
					</div>
                    <article class="single-university-text">
							<p>
								<?php
									while ( have_posts() ) : the_post();

										the_content();

									endwhile; // End of the loop.
									?>
							</p><br>
                    </article>
					 <div class="speciality-deps">
					 	<h3>Специальности</h3>
	 							<table class="table table-bordered table-hover responsive">
	 								<thead>
	 									<tr>
	 							 	  		<th rowspan="2"> <h4><?php _e( 'название факультета', 'aguaviva' )?></h4> </th>
	 							 	  		<th colspan="3" rowspan="1"><h4><?php _e( 'бакалаврат', 'aguaviva' )?></h4></th>
	 							 	  		<th colspan="3" rowspan="1"><h4><?php _e( 'магистратура', 'aguaviva' )?></h4></th>
											<tr>
												<td colspan="1"> <p><?php _e( 'стоимость в год', 'aguaviva' )?></p> </td>
												<td colspan="1"> <p><?php _e( 'срок', 'aguaviva' )?></p> </td>
												<td colspan="1"> <p><?php _e( 'предметы и количество часов', 'aguaviva' )?></p> </td>
												<td colspan="1"> <p><?php _e( 'стоимисть в год', 'aguaviva' )?></p> </td>
												<td colspan="1"> <p><?php _e( 'срок', 'aguaviva' )?></p> </td>
												<td colspan="1"> <p><?php _e( 'предметы и количество часов', 'aguaviva' )?></p> </td>
											</tr>
	 							 	 	</tr>
	 								</thead>
	 								<tbody>
	 									<?php foreach ($subjects as $subject) : ?>
	 										<tr>
	 								   			<td> <h4><?php echo $subject["department"];?></h4> </td>
	 											<td> <h4><?php echo $subject["bachelor_price"];?></h4> </td>
	 											<td> <h5><?php echo $subject["bachelor_time"];?></h5> </td>
	 											<td> <a class="<?php echo ( $subject["bachelor_pdf"] == '') ? 'icon-pdf-none' : 'icon-pdf';?>" href="<?php echo $subject["bachelor_pdf"];?>"> <p><?php _e( 'Скачать .PDF', 'aguaviva' )?></p></a> </td>
	 											<td> <h4><?php echo $subject["master_price"];?></h4> </td>
	 											<td> <h5><?php echo $subject["master_time"];?></h5> </td>
	 											<td> <a class="<?php echo ( $subject["bachelor_pdf"] == '') ? 'icon-pdf-none' : 'icon-pdf';?>" href="<?php echo $subject["master_pdf"];?>"> <p><?php _e( 'Скачать .PDF', 'aguaviva' )?></p></a> </td>
	 								  		</tr>
	 									<?php endforeach; ?>
	 								</tbody>
	 							</table>
					 </div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

	<!-- contact us -->
	<?php get_template_part( 'template-parts/content', 'contact-us' ); ?>
	<!-- .contact us -->

<?php
get_footer();
