<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */
 	global $wp_query;

   $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;

	 $qry_args = array(
			'post_status'    => 'publish',
			'post_type'      => 'recordings',
			'posts_per_page' => 9,
			'paged'          => $paged
		);

		$query = new WP_Query( $qry_args );
    $tmp_query = $wp_query;
		$wp_query = null;
    $wp_query = $query;

get_header(); ?>
	<div class="universities-head">
		<div class="container">
			<h1><?php echo get_field('blog'); ?></h1>
			<p><?php echo get_field('blog-description'); ?></p>
		</div>
	</div>
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
				<div class="container blog">
					<?php if ($wp_query->have_posts()) :
			    while ($wp_query->have_posts()) : $wp_query->the_post(); ?>
								<div class="col-xs-12 col-sm-6 col-md-4">
									<div class="blog-loop">
										<span class="theme-color-font icon-date-hover icon-date icon-blog"><?php echo get_the_date();?></span>
										<span class="theme-color-font icon-comments"><?php echo get_comments_number(); ?></span>
										<span class="theme-color-font icon-view"><?php if(function_exists('bac_PostViews')) {
											    echo get_post_meta(get_the_ID(), 'post_views_count', true);
											}?>
										</span>

											<?php the_post_thumbnail( array(300,) ); ?>

										<div class="wrap-single-blog ">
											<div class="content-cut">
												<h4><?php echo get_the_title();?></h4>
												<p><?php echo get_the_content();?></p>
											</div>
											<a class="btn-style-form btn-search-margin-center" href="<?php echo get_post_permalink();?>"><?php _e( 'подробнее', 'aguaviva' )?></a><br>
										</div>
									</div>
						 		</div>
			    	<?php endwhile; ?>

					<?php
					  previous_posts_link('&laquo; Предыдущие посты');
			    	next_posts_link( 'Следующие посты &raquo;', $query->max_num_pages );
			    	wp_reset_postdata();
						else : ?>
			       		<?php _e( 'Нет постов для отображения', 'aguaviva' )?>
						<?php	endif; ?>
		</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
$wp_query = null;
$wp_query = $tmp_query;

get_footer();
