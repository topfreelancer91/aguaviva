<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package aguaviva
 */
// переменные ACF
		get_field('field_577df56ed2bda');
		get_field('field_577df67f8b891');
		get_field('field_577df871b6554');

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<!-- <div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ru_RU/sdk.js#xfbml=1&version=v2.7";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script> -->
<div id="page" class="wrapper">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php if ( get_theme_mod( 'm1_logo' ) ) : ?>
			    <a class="col-xs-3 col-md-3" href="<?php echo esc_url( home_url( '/' ) ); ?>" id="site-logo" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">

			        <img src="<?php echo get_theme_mod( 'm1_logo' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">

			    </a>

		    <?php else : ?>

			    <hgroup>
			        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			        <p class="site-description"><?php bloginfo( 'description' ); ?></p>
			    </hgroup>

			<?php endif; ?>
				    <ul class="top-contact col-xs-5 col-xs-offset-2 col-md-offset-0 col-md-8 clearfix">
						<li class="hidden-xs col-sm-4 col-md-4">
							<a href="skype:<?php echo the_field('skype'); ?>"><span class="skype">skype </span><span class="hidden-xs hidden-sm hidden-md">: <?php echo the_field('skype'); ?></span></a>
						</li>
						<li class="hidden-xs col-sm-4 col-md-4">
							<a href="tel:<?php echo the_field('viber'); ?>"><span class="viber">viber </span><span class="hidden-xs hidden-sm hidden-md">: <?php echo the_field('viber'); ?></span></a>
						</li>
						<li class="hidden-xs col-sm-4 col-md-4">
							<a href="mailto:<?php echo the_field('mail'); ?>"><span class="mail">e-mail </span><span class="hidden-xs hidden-sm hidden-md">: <?php echo the_field('mail'); ?></span></a>
						</li>
					</ul>

						<?php if ( is_active_sidebar( 'lang' ) ) : ?>
							<div id="lang" class="col-xs-2 col-sm-2 col-md-1 pull-right">
								<?php dynamic_sidebar( 'lang' ); ?>
							</div>
						<?php endif; ?>
				</div><!-- .site-branding -->
		<div class="full-width nav-wrapper clearfix">
		<nav id="nav" class="container navbar navbar-default" role="navigation">
		  <!-- toggle get grouped for better mobile display -->
		  <div class="navbar-header visible-xs">
		    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
		      <span class="sr-only">Toggle navigation</span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		      <span class="icon-bar"></span>
		    </button>
		  </div>

		  <!-- Collect the nav links, forms, and other content for toggling -->
		  <div class="collapse navbar-collapse navbar-main-collapse">
		    <?php wp_nav_menu(array(
		      'container_class' => 'menu-header',
		      'theme_location' => 'primary',
		      'items_wrap' => '<ul id="%1$s" class="%2$s nav navbar-nav">%3$s</ul>',
		      'walker' => new Bootstrap_Walker_Nav_Menu,
		    )); ?>
		  </div><!-- /.navbar-collapse -->
		</nav>
		</div>
	</header><!-- #masthead -->

	<div id="content">
