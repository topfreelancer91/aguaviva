<?php
/**
 * The template for displaying archive pages.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */
// переменные ACF
		get_field('field_578c8addd38d3');




get_header(); ?>
<?php
	$POST_ID = get_the_ID();
?>
	<div class="students-head">
		<div class="overlay-img">
			<div class="container">
				<h1><?php echo get_the_title(); ?></h1>
				<p><?php echo the_field('students_title'); ?></p>
			</div>
		</div>
	</div>


	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<!-- comments -->
			<div class="comment-students">
				<?php get_template_part( 'template-parts/content', 'comments' ); ?>
			</div>
			<!-- .comments -->
			<!-- video -->
			<?php get_template_part( 'template-parts/content', 'video' ); ?>
			<!-- .video -->
			<!-- photo -->
			<?php echo do_shortcode( '[FinalTilesGallery id="1"]' );?>
			<!-- .photo -->
			<div class="container">
			<?php echo get_post_field('post_content', $POST_ID); ?>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
