jQuery(function($) {
// Mask telephone input
$('input[name=tel]').mask('+(999) 999-999-999');

// Function to remove error border on click
$('input, textarea').bind('click', function(){
  $(this).removeClass('validation-error');
});
// block <a> pdf
$('.icon-pdf-none').on('click', function(event){
  event.preventDefault();
});

// style select
 $('select').select2();
 $("#level").select2({
  placeholder: objectL10n.level,
  allowClear: true,
  minimumResultsForSearch: Infinity
});

 $("#alphabet").select2({
  placeholder: objectL10n.sort,
  allowClear: true,
  minimumResultsForSearch: Infinity
});

$("#city").select2({
  placeholder: objectL10n.place,
  allowClear: true,
});

$("#department").select2({
  placeholder: objectL10n.subject,
  allowClear: true,
});

$('#search').select2({
  allowClear: true,
  placeholder: objectL10n.name_university,
});


// Contact form event to send ajax in function php
$('#contact_us').on('click', function (){

    if(validateForm( '#contact' )){

      var name = $('#contact input[name=name]').val();
      var telephone = $('#contact input[name=tel]').val();
      var email = $('#contact input[name=mail]').val();
      var text = $('#contact textarea[name=text]').val();

      $.ajax({
 			 type: 'POST',
 			 url: ajax_object.ajax_url,
 			 data: {
 					 action    : 'contact_us',
 					 nonce     : ajax_object.nonce,
 					 name      : name,
           telephone : telephone,
           email     : email,
           text      : text
 			 },
 			 success: function( data ) {
        // HERE WELL BE SUCCESS
        $('#thankyouModal').modal('show');
 			 }
 		 });

    }
});
// END OF BLOCK CODE


//Consultation form event to send ajax in function php
$('#consult_submit').on('click', function(){
    if(validateForm( '#take-consult' )){
      var name = $('#take-consult input[name=name]').val();
      var telephone = $('#take-consult input[name=tel]').val();
      var email = $('#take-consult input[name=mail]').val();

      $.ajax({
 			 type: 'POST',
 			 url: ajax_object.ajax_url,
 			 data: {
 					 action    : 'get_consultation',
 					 nonce     : ajax_object.nonce,
 					 name      : name,
           telephone : telephone,
           email     : email,
 			 },
 			 success: function( data ) {
        // HERE WELL BE SUCCESS
        $('#consultation-modal').modal('hide');
        $('#thankyouModal').modal('show');
 			 }
 		 });

    }
});
// END OF BLOCK CODE

//Booking form event to send ajax in function php
$('#booking_submit').on('click', function(){
    if(validateForm( '#take-booking' )){
      var name = $('#take-booking input[name=name]').val();
      var telephone = $('#take-booking input[name=tel]').val();
      var email = $('#take-booking input[name=mail]').val();

      $.ajax({
 			 type: 'POST',
 			 url: ajax_object.ajax_url,
 			 data: {
 					 action    : 'enroll',
 					 nonce     : ajax_object.nonce,
 					 name      : name,
           telephone : telephone,
           email     : email,
 			 },
 			 success: function( data ) {
        // HERE WELL BE SUCCESS
        $('#booking-modal').modal('hide');
        $('#thankyouModal').modal('show');
 			 }
 		 });

    }
});
// END OF BLOCK CODE


// Function to validate form with inputs
function validateForm(form) {

   var errors = 0;
   var regularEmail = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);


    $(form).find("input, select, textarea").each(function(){
       var input = $(this);


        if( !input.val() && input.attr('data-required') && input.attr('type') != "mail"){
           input.addClass('validation-error');
           errors++;
        }  else if ( input.attr('type') == "mail" && !regularEmail.test( input.val() ) ) {
          input.addClass('validation-error');
          errors++;
        } else {
           input.removeClass('validation-error');
        }
    });

    if (errors != 0 ){
      return false;
    } else return true;

}
// END OF BLOCK CODE


// Funcion seach on main page
 $('#getUniversity').on('click', function (){

   var subject = $('#department option:selected').val();
   var city = $('#city option:selected').val();
   var level = $('#level option:selected').val();
   var alphabet = $('#alphabet option:selected').val();

   console.log(subject, city, level);

   $.ajax({
    type: 'POST',
    url: ajax_object.ajax_url,
    data: {
        action    : 'get_sorted_universities',
        nonce     : ajax_object.nonce,
        level     : level,
        place     : city,
        subject   : subject,
    },
    success: function( data ) {

     data = JSON.parse(data);

     if (alphabet == 1) {
       data.sort(sortByTitle);
     } else if (alphabet == 2) {
       data.sort(sortByTitle);
       data.reverse();
     }

     $('#search-result').html('');

     if(data.length != 0){

     data.forEach(function(item, i, arr) {
        var html = "";
        html += '<div class="col-xs-12 col-sm-6 col-md-4">';
        html += '<div class="university-loop">';
        html += '<span class="theme-color-font icon-point-hover point icons-university">'+data[i].place+'</span>';
        html += '<span class=" icon-hat level icons-university">'+data[i].level+'</span>';
        html += '<span class=" icon-star rating icons-university">'+data[i].rating+'</span>';
        html += '<img class="thumbnail-img" src="'+data[i].thumbnail+'">';
        html += '<div class="wrap-single-university">';
        html +=  '<h4>'+data[i].title+'</h4>';
        html += '<a class="btn-style-form btn-search-margin-center" href="'+data[i].link+'">'+objectL10n.read_more+'</a><br>';
        html += '</div>'
        html += '</div>'
        html += '</div>'

        $('#search-result').append(html);
     });

   } else {
       $('#search-result').append("<h2>По вашему запросу ничего не найдено :(</h2>");
   }

    }
  });

    $('.search-result').removeClass('hidden');
 });


 // Sort subjects by title
 function sortByTitle(post_1 , post_2){
    if(post_1.title < post_2.title) { return -1;
    }else if(post_1.title > post_2.title) {return 1;
    }else return 0;
 }


 $("#search").on('change', function(){
    var selectedUnivesity =  $('#search option:selected').val();
    if(selectedUnivesity !== ''){
        $("#goToPost").attr('href' , selectedUnivesity);
    } else {
        $("#goToPost").attr('href' , 'javascript:void(0)');
    }
 });

// modal consultation
$('#consultation').on('click', function (){
  $('#consultation-modal').modal('show');
});
// modal booking-modal
$('#booking').on('click', function (){
  $('#booking-modal').modal('show');
});
 $('.close').on('click', function(){
    localStorage.setItem('closet-social', 'true');
 });
// modal booking-modal
        $(window).scroll(function(){
            var bo = $("body").scrollTop();
            var closet = localStorage.getItem('closet-social');
            //console.log(closet);
            if ( bo > 100 && !closet ) {
              $("#subscribe-modal").modal('show');
            } else {
               $("#subscribe-modal").modal('hide');
            }
    });
// modal documents img
$('.owl-item > div').on('click', function (){
  $('#documents-modal').modal('show');
  var slideBgi = $(this).css('backgroundImage');
  console.log(slideBgi);
  $('.full-img').css('backgroundImage', slideBgi);
});

//table responsive
$('table').addClass('responsive');
$(document).ready(function() {
  var switched = false;
  var updateTables = function() {
    if (($(window).width() < 1024) && !switched ){
      switched = true;
      $("table.responsive").each(function(i, element) {
        splitTable($(element));
      });
      return true;
    }
    else if (switched && ($(window).width() > 1024)) {
      switched = false;
      $("table.responsive").each(function(i, element) {
        unsplitTable($(element));
      });
    }
  };

  $(window).load(updateTables);
  $(window).on("redraw",function(){switched=false;updateTables();}); // An event to listen for
  $(window).on("resize", updateTables);


  function splitTable(original)
  {
    original.wrap("<div class='table-wrapper' />");

    var copy = original.clone();
    copy.find("td:not(:first-child), th:not(:first-child), thead>tr>td:first-child").css("display", "none");
    copy.removeClass("responsive");

    original.closest(".table-wrapper").append(copy);
    copy.wrap("<div class='pinned' />");
    original.wrap("<div class='scrollable' />");

    setCellHeights(original, copy);
  }

  function unsplitTable(original) {
    original.closest(".table-wrapper").find(".pinned").remove();
    original.unwrap();
    original.unwrap();
  }

  function setCellHeights(original, copy) {
    var tr = original.find('tr'),
        tr_copy = copy.find('tr'),
        heights = [];

    tr.each(function (index) {
      var self = $(this),
          tx = self.find('th, td');

      tx.each(function () {
        var height = $(this).outerHeight(true);
        heights[index] = heights[index] || 0;
        if (height > heights[index]) heights[index] = height;
      });

    });

    tr_copy.each(function (index) {
      $(this).height(heights[index]);
    });
  }

});


});
//slider university
  jQuery(document).ready(function($) {

        $('#gallery-university').carousel({
                interval: false
        });
        $('#comments-slider').carousel({
                interval: false
        });
        $('#gallery-university-thumb').carousel({
                interval: false
        });


        //Handles the carousel thumbnails
       $('[id^=carousel-selector-]').click( function(){
            var id = this.id.substr(this.id.lastIndexOf("-") + 1);
            var id = parseInt(id);
            $('#gallery-university').carousel(id);
        });


});
