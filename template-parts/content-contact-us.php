<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF
		get_field('field_577cc655a49bb');
		get_field('field_577cc6f5b7684');
		get_field('field_577df56ed2bda');
		get_field('field_577df67f8b891');
		get_field('field_577df871b6554');
		get_field('field_578cd653c2716');
		get_field('field_578cd6b4c2717');
		get_field('field_578ce1d73be4c');


?>
<section class="contact-us">
	<div class="container">
		<h3><?php echo the_field('contact-us_h3'); ?></h3>
		<p><?php echo the_field('contact-us_title'); ?></p>
		<div class="row">
			<div class="modal fade" id="thankyouModal" tabindex="-1" role="dialog" aria-labelledby="thankyouLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header success">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title" id="thankyouLabel"><?php echo the_field('success_submit_h3'); ?></h4>
			            </div>
			            <div class="modal-body">
			                <p><?php echo the_field('success_submit_p'); ?></p>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="modal fade" id="warningModal" tabindex="-1" role="dialog" aria-labelledby="warningLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header warning">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title" id="myModalLabel"><?php _e( 'Сообщение НЕ отправлено', 'aguaviva' )?></h4>
			            </div>
			            <div class="modal-body">
			                <p><?php _e( 'Попробуйте через 5 минут', 'aguaviva' )?></p>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="modal fade" id="consultation-modal" tabindex="-1" role="dialog" aria-labelledby="consultationLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title" id="myModalLabel"><?php echo the_field('consultation_h3'); ?></h4>
			            </div>
			            <div class="modal-body clearfix">
				            <form action="" id="take-consult" class="clearfix">
								<div class="col-xs-12">
									<input type="text" name="name" data-required="true" placeholder="<?php _e( 'ВАШЕ ИМЯ', 'aguaviva' )?>">
									<input type="text" name="tel" data-required="true" placeholder="<?php _e( 'ВАШ ТЕЛЕФОН', 'aguaviva' )?>">
									<input type="mail" name="mail" data-required="true" placeholder="<?php _e( 'ВАШ E-MAIL', 'aguaviva' )?>">					
									<a href="javascript:void(0)" id="consult_submit" class="btn-style-form"><?php _e( 'получить консультацию', 'aguaviva' )?></a>
								</div>
							</form>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="modal fade" id="booking-modal" tabindex="-1" role="dialog" aria-labelledby="bookingLabel" aria-hidden="true">
			    <div class="modal-dialog">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                <h4 class="modal-title" id="bookingLabel"><?php echo the_field('consultation_h3'); ?></h4>
			            </div>
			            <div class="modal-body clearfix">
				            <form action="" id="take-booking" class="clearfix">
								<div class="col-xs-12">
									<input type="text" name="name" data-required="true" placeholder="<?php _e( 'ВАШЕ ИМЯ', 'aguaviva' )?>">
									<input type="text" name="tel" data-required="true" placeholder="<?php _e( 'ВАШ ТЕЛЕФОН', 'aguaviva' )?>">
									<input type="mail" name="mail" data-required="true" placeholder="<?php _e( 'ВАШ E-MAIL', 'aguaviva' )?>">					
									<a href="javascript:void(0)" id="booking_submit" class="btn-style-form"><?php _e( 'записаться', 'aguaviva' )?></a>
								</div>
							</form>
			            </div>
			        </div>
			    </div>
			</div>
			<div class="col-xs-12 col-sm-3">
				<hgroup>
			        <h3 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h3>
			    </hgroup>
			    <ul>
					<li>
						<a href="skype:<?php echo the_field('skype'); ?>"><span class="skype"></span><?php echo the_field('skype'); ?></a>
					</li>
					<li>
						<a href="tel:<?php echo the_field('viber'); ?>"><span class="viber"></span><?php echo the_field('viber'); ?></a>
					</li>
					<li>
						<a href="mailto:<?php echo the_field('mail'); ?>"><span class="mail"></span><?php echo the_field('mail'); ?></a>
					</li>
				</ul>
			</div>

			<form action="" id="contact">
				<div class="col-xs-12 col-sm-4">
					<input type="text" name="name" data-required="true" placeholder="<?php _e( 'ВАШЕ ИМЯ', 'aguaviva' )?>">
					<input type="text" name="tel" data-required="true" placeholder="<?php _e( 'ВАШ ТЕЛЕФОН', 'aguaviva' )?>">
					<input type="mail" name="mail" data-required="true" placeholder="<?php _e( 'ВАШ E-MAIL', 'aguaviva' )?>">
				</div>
				<div class="col-xs-12 col-sm-5">
					<textarea name="text" data-required="true" placeholder="<?php _e( 'ТЕКСТ СООБЩЕНИЯ', 'aguaviva' )?>"></textarea>
					<a href="javascript:void(0)" id="contact_us" class="btn-style-form"><?php _e( 'написать нам', 'aguaviva' )?></a>
				</div>
			</form>
		</div>
	</div>
</section>
