<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF 
		get_field('field_578c9ec4ec566');
		get_field('field_578c9f30f1127');
?>
<section class="comments">
	<div class="container">
		<h3><?php echo the_field('video'); ?></h3>
		<p><?php echo the_field('video_title'); ?></p>
		<div class="row">
			<?php echo do_shortcode( '[slide-anything id="105"]' );?>
		</div>
	</div>
</section>