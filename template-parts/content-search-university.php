<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF
		get_field('field_577a3efe6dcd1');
		get_field('field_577a3d61d2004');

$levels = get_field_object("field_5786168661af4")["choices"];
$cityes = get_field_object("field_578618d7f983a")["choices"];
$allSubjects = getAllUnicSubjects();

$qry_args = array(
	'post_status' => 'publish',
	'post_type' => 'universities',
	'posts_per_page' => -1,
);

$all_universities = new WP_Query( $qry_args );

?>
	<div class="container">
		<h3><?php echo the_field('university_search'); ?></h3>
		<p>
		<?php if(is_front_page()){
			echo the_field('university_search_title');
			}?>
		</p>
		<div class="row border-bottom">
			<h4 class="col-xs-12 theme-color-font"><?php _e( 'введите название вуза', 'aguaviva' )?>:</h4>
			<form action="">
				<div class="col-xs-12 col-sm-10">
				<select name="" id="search">
						<option></option>
						<?php foreach($all_universities->posts as $university) : ?>
							<option value="<?php echo get_permalink($university->ID); ?>"><?php echo get_the_title($university->ID); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-2">
					<a href="#" id="goToPost" class="btn-style-form"><?php _e( 'перейти', 'aguaviva' )?></a>
				</div>
			</form>
		</div>
		<div class="row padding-top">
			<h4 class="col-xs-12 theme-color-font"><?php _e( 'выберите нужные параметры', 'aguaviva' )?>:</h4>
			<form action="">
				<div class="col-xs-12 col-sm-6 col-md-6">
					<select name="" id="department">
						<option></option>
						<?php foreach($allSubjects as $subject) : ?>
							<option value="<?php echo $subject->departmentID; ?>"><?php echo $subject->department; ?></option>
						<?php endforeach; ?>
					</select>
				</div>

				<div class="col-xs-12 col-sm-6 col-md-6">
					<select name="" id="level">
						<option></option>
						<?php foreach($levels as $level) : ?>
							<option value="<?php echo $level; ?>" <?php echo ($level == $_GET['level']) ? 'selected' : ''; ?>><?php echo $level; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<select name="" id="city">
						<option></option>
						<?php foreach($cityes as $city) : ?>
							<option value="<?php echo $city; ?>" <?php echo ($city == $_GET['place']) ? 'selected' : ''; ?>><?php _e( $city , 'aguaviva'); ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-6">
					<select name="" id="alphabet">
						<option></option>
						<option value="1"><?php _e( 'А - Я', 'aguaviva' )?></option>
						<option value="2"><?php _e( 'Я - А', 'aguaviva' )?></option>
					</select>
				</div>
				<div class="col-xs-12">
					<a href="javascript:void(0)" id="getUniversity" class="btn-style-form btn-search-margin-center"><?php _e( 'подобрать вуз', 'aguaviva' )?></a>
				</div>
			</form>
		</div>
	</div>
