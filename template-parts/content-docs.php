<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF 
		get_field('field_577d29dcd76fc');
		get_field('field_577d327b38899');

		
?>
<section class="documents">
	<div class="container">
		<h3><?php echo the_field('docs_h3'); ?></h3>
		<div class="row">
			<?php echo do_shortcode( '[slide-anything id="49"]' ); ?>
		</div>
	</div>
</section>