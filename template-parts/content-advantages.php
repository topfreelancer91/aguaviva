<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF 
		get_field('field_577bc1d10b21e');

		
?>
<section class="advantages">
	<div class="container">
		<h3><?php echo the_field('advantages_h3'); ?></h3>
		<div class="row">
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p class="sprite-tarelka-padding col-xs-6">
					<span class="sprite sprite-tarelka "></span>
				</p>
				<figure class="col-xs-6">
					<span class="theme-color-font">~3$</span>
					<p><?php _e( 'Средний чек', 'aguaviva' )?></p>
				</figure>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p class="sprite-tarelka-padding col-xs-6">
					<span class="sprite sprite-house "></span>
				</p>
				<figure class="col-xs-6">
					<span class="theme-color-font">~20$</span>
					<p><?php _e( 'Аренда квартиры', 'aguaviva' )?></p>
				</figure>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p class="sprite-tarelka-padding col-xs-6">
					<span class="sprite sprite-car "></span>
				</p>
				<figure class="col-xs-6">
					<span class="theme-color-font">~4$</span>
					<p><?php _e( 'Услуги такси', 'aguaviva' )?></p>
				</figure>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p class="sprite-bus-padding col-xs-6">
					<span class="sprite sprite-bus "></span>
				</p>
				<figure class="col-xs-6">
					<span class="theme-color-font">~1$</span>
					<p><?php _e( 'Услуги общ. транспорта', 'aguaviva' )?></p>
				</figure>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p class="sprite-film-padding col-xs-6">
					<span class="sprite sprite-film "></span>
				</p>
				<figure class="col-xs-6">
					<span class="theme-color-font">~5$</span>
					<p><?php _e( 'Поход в кинотеатр', 'aguaviva' )?></p>
				</figure>
			</div>
			<div class="col-xs-12 col-sm-6 col-md-4">
				<p class="sprite-tarelka-padding col-xs-6">
					<span class="sprite sprite-tiket "></span>
				</p>
				<figure class="col-xs-6">
					<span class="theme-color-font">~2$</span>
					<p><?php _e( 'Билет в музей', 'aguaviva' )?></p>
				</figure>
			</div>
		</div>
	</div>
</section>