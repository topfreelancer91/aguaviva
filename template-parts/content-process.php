<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF 
		get_field('field_577a560abb14c');
		get_field('field_577a564abb14d');
		get_field('field_577a5ff9ff5ab');
		get_field('field_577a61ec72c03');
		get_field('field_577a7e2b4b0ca');
		get_field('field_577a7ea3ea414');
		get_field('field_577a7f687e852');
		get_field('field_577b8586fdc5c');
		get_field('field_577b86f6f2fe5');
		get_field('field_577b8760ccf0d');
		get_field('field_577b87d7b5484');
		get_field('field_577b881a2c274');
		get_field('field_577b9b2c6b226');

		
?>
<section class="process">
	<div class="container">
		<h3><?php echo the_field('process_header'); ?></h3>
		<p><?php echo the_field('process_description'); ?></p>
		<div class="row">
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/bill.png" alt="" class="icon-bill">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('pick_cource'); ?></h4>
				<p><?php echo the_field('pick_cource_p'); ?></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/speech-bubble.png" alt="" class="icon-speech-bubble">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('speech-bubble'); ?></h4>
				<p><?php echo the_field('speech-bubble_p'); ?></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/mail-envelope-opened.png" alt="" class="icon-mail-envelope-opened">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('mail-envelope-opened'); ?></h4>
				<p></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/wallet.png" alt="" class="icon-wallet">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('wallet'); ?></h4>
				<p></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/hand-gesture.png" alt="" class="icon-hand-gesture">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('hand-gesture'); ?></h4>
				<p></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/airplane.png" alt="" class="icon-airplane">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('airplane'); ?></h4>
				<p></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/wallet.png" alt="" class="icon-wallet2">
					<img src="wp-content/themes/aguaviva/img/dot_arrow.png" alt="" class="icon-dot_arrow">
				</figure>
				<h4><?php echo the_field('wallet2'); ?></h4>
				<p></p>
			</div>
			<div class="col-xs-12 col-sm-4 col-md-3">
				<figure>
					<img src="wp-content/themes/aguaviva/img/notebook.png" alt="" class="icon-notebook">
					<img src="wp-content/themes/aguaviva/img/dots.png" alt="" class="icon-dots">
				</figure>
				<h4><?php echo the_field('notebook'); ?></h4>
				<p></p>
			</div>
		</div>
	</div>
	<div class="process-box2">
		<div class="overlay-img">
			<div class="container">
				<div class="col-xs-12 col-md-8"><?php echo the_field('diplom'); ?></div>
				<div class="col-xs-12 col-md-4">
					<a href="javascript:void(0)" id="booking" class="btn-style-form"><?php _e( 'записаться', 'aguaviva' )?></a>
				</div>
			</div>
		</div>
	</div>
</section>