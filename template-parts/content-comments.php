<?php
/**
 * Template part for displaying results in search university.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF
		get_field('field_577e55face128');
		get_field('field_578c9c7833ecd');
		$slider_coments = get_field('slider_coments', 2);
		$i = 0;
?>
<section class="comments">
	<div class="container">
	<h3><?php if(is_front_page()){
			echo the_field('comments_h3');
			}
		else{
			echo the_field('comments');
		}?></h3>

		<div class="row">
			<!-- <?php echo do_shortcode( '[slide-anything id="52"]' );?> -->
			<?php echo do_shortcode( "[slide-anything id='307']" );?>



		</div>
	</div>
</section>
