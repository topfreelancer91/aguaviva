<?php
/**
 * Template part for displaying page content in page.php.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="slide-fw">
		<div class="overlay-img">
			<header class="entry-header">
				<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					<h4><?php echo get_field('field_577a0595c929d' , $post->ID); ?></h4>
					<a href="javascript:void(0)" id="consultation" class="btn-style-form"><?php _e( 'получить консультацию', 'aguaviva' )?></a>	
				</div>
			</header><!-- .entry-header -->
		</div>
	</div>

	<div class="entry-content container" style="text-align: center;">

		<?php query_posts('page_id=168');
		 if ( have_posts() ) : while ( have_posts() ) : the_post(); global $more; $more = 0; ?>

			  <h3><?php the_title(); ?></h3>
			    <div class="entry">
			   		 <?php the_content('<a class="btn-read" href="'. get_permalink() .'"> '. __( "читать", "aguaviva" ).'</a>'); ?>
			    </div>
			<?php endwhile; endif; wp_reset_query(); ?>
	</div><!-- .entry-content -->
	<!-- search university -->
	<section class="search-university"><div class="overlay-img">
		<?php get_template_part( 'template-parts/content', 'search-university' ); ?>
		</div></section>
		<div class="search-result hidden container clearfix">
		<h3><?php _e( 'Результаты поиска', 'aguaviva' )?></h3>
		<p><?php _e( 'Lorem под результатами поиска', 'aguaviva' )?></p>
			<div id="search-result" class="row">
				<div class="col-xs-12 col-sm-6 col-md-4">

				</div>
			</div>
		</div>

	<!-- .search university -->
	<!-- process -->
	<?php get_template_part( 'template-parts/content', 'process' ); ?>
	<!-- .process -->
	<!-- advantages -->
	<?php get_template_part( 'template-parts/content', 'advantages' ); ?>
	<!-- .advantages -->
	<!-- comments -->
	<?php get_template_part( 'template-parts/content', 'comments' ); ?>
	<!-- .comments -->
	<!-- contact us -->
	<?php get_template_part( 'template-parts/content', 'contact-us' ); ?>
	<!-- .contact us -->
	<!-- docs -->
	<?php get_template_part( 'template-parts/content', 'docs' ); ?>
	<!-- .docs -->
	<!-- modal img -->
		<?php get_template_part( 'template-parts/content', 'modal' ); ?>
	<!-- .modal img -->

	<?php if ( get_edit_post_link() ) : ?>
		<footer class="entry-footer">
			<?php
				edit_post_link(
					sprintf(
						/* translators: %s: Name of current post */
						esc_html__( 'Edit %s', 'aguaviva' ),
						the_title( '<span class="screen-reader-text">"', '"</span>', false )
					),
					'<span class="edit-link">',
					'</span>'
				);
			?>
		</footer><!-- .entry-footer -->
	<?php endif; ?>
</article><!-- #post-## -->
