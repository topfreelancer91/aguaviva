<?php
/**
 * Template part for displaying results modal img document.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package aguaviva
 */

// переменные ACF 
		get_field('field_578c9ec4ec566');
		get_field('field_578c9f30f1127');
?>
	<div class="modal fade" id="documents-modal" tabindex="-1" role="dialog" aria-labelledby="imgLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	            </div>
	            <div class="modal-body clearfix">
						<div class="col-xs-12">
							<div class="full-img"></div>
						</div>
					</form>
	            </div>
	        </div>
	    </div>
	</div>
	<div class="modal fade" id="subscribe-modal" tabindex="-1" role="dialog" aria-labelledby="subscribeLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	            <p class="press-like"><?php _e( 'Нажмите “Нравится”,', 'aguaviva' )?></p>
	            <p class="read-fb"><?php _e( 'и читайте нас на Facebook', 'aguaviva' )?></p>
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	            </div>
	            <div class="modal-body clearfix">
						<div class="col-xs-12">
							<div class="subscribe-box">
								<div class="fb-follow" data-href="https://www.facebook.com/aguavivaetudier/" data-layout="standard" data-size="large" data-show-faces="true">
								</div>
							</div>
						</div>
					</form>
	            </div>
	        </div>
	    </div>
	</div>
